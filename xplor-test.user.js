// ==UserScript==
// @name         Xplor Test
// @namespace    https://openuserjs.org/users/parlor.trickss
// @author       parlortricks
// @description  Download all the photos of your child from the selected observation
// @homepageURL  https://openuserjs.org/scripts/parlor.trickss/Xplor_Album_Download
// @updateURL    https://openuserjs.org/install/parlor.trickss/Xplor_Album_Download.user.js
// @icon         data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAALcUExURQAAAOWirPecQ/ejUtl1hNl6iduKlvWaqvJ7kOh+kPJ3jfFxiMxebslSY+Zzh/u/U+Vrcfu6Rvu8S/WQQMlSVu9Zc+1JZu5QbPu0N+Faafu0N8BCVPqxL8FFVvWINPqwLPWJNddSZ/qyMr45TPWKNu1KZvqyMPWINPCBNfqxLr89UL89UPqyML89T78+Ub89TO1FYut6U/WHMr48T789UMBAUt9NYu1FYvCBNu1FYt9LYvGMPb4+UN9OZvigNL48TvWGMfSGMO1CYPSFL706Tb05TPSELfquJ705Su1HZPquJ/SDLL04S748T95IYL06TfSDLb04S/qtJL04S8Q8UOxAXvSDLOw+XMQ7TstMQ705S+1BX+w+XexAXt5IYOw/Xu1EXL04Suw/XfqtI/qtI95HYN5IYfqtI/SCK+w+XOw+XdhFXew/Xbw3St5IX/qsIPqsIfmlJOw+XLw3SfSBKrw3SfqsIPqsIdxGXr43S+x2LvSBKfSAKfqrH/qrHhivvRmWpByvuh+ZqCO0wiavsyuSly2jsjCtuziRkDikszymtT+uoUSQiUS/y06PhFaukWLJ03TP2Hate3mLbH2Lan2tdoqtbIzL2piIW5vS4aSoWaiHUq6HT7Lc67Xe7bXl67s1SLw2Sb04R72sSb86RsE4TMI+RMQ5TsQ9SMTk88VCQ8ZDQsc6UMg7UMs8U8uEP8w8U8xMP88+Vc9OPdDq+dM/V9NUO9OsOdZBWddaOdhbONpeNttDXdtJV9vv+txEXt1EXt5GXd/x++GrMOJXUuJoM+NqMeRtP+Tz/OT29+VsMeX2+OhsReiBL+lwQ+pzLup0QOt0Lew7Wux4Pu1AWO5IU+5/OfCAKvCINPJoQvJvPvJ9KfJ+KfKMLPN+KfN/KfSAKPSBKPSCKPSEJ/SVLPT7/PWrIfeTK/eiJPerIPicIvigIfmiIPmiI/mpH/qqHvqrHv3+//7//////2jCoCYAAAB/dFJOUwAAAAABAQEBAwQEBQcICAgKCg0OEhobHiMlJis0NTU3ODo6PT5BQURHSE1PUFNaXWBkZWdnZ2dpa21vdHZ2e32ChIWOkpOYmp6foaWoqKqrq6+vsLCyv8THx8jIyszNztHS0tPW2Nrd3+Dg5+jp6urt7/Dx8/T09fb5+fn9/f5FKeZpAAAB0ElEQVQYGX3BY2McURQG4Le2bdu2bdu2zdS23VOmTZ1ujUxqY+qmTU/t5jT5A507Nx86O7v7PPAvS9WWAyZ3hh+pK/Y0DCMseBx8SlVtpKEcIYIvRQbJp2uGYRwnIrilaTRTRP48CQ8loglwydxPtLchRNQH3nKNEpHvX0R+fYg4TdQYXgpOEjmzcnbQ0i0fmfn63lJwyjtR5Oq81bc/71ywli1T08Mh62gR2bA8Oubv5R2zjjFzazik7S+W9atioq94PIs3MXN+ODQVZc/cu7c8noNztjF3hENx0dYs3Lh985JlL3h6TsRKXKBuh+69x4j2Y+uK+YvW3WduCC1ppSGm5bVoP9+zNjgdbBm7mcrT36J8u/mQtWm5Ycs01LS9EuXlpQORrJWFLVkPU/sqIlGPLu47z1odaFVMc3ivTn3HPhORqHthwfsj2FYvHrQmpTPAkqimiDwwDtMdttWOCy/Z2854bhylc6y8qQwfyoSH0klWHh8qCbeUA0/RiUhmfndhNzWAW42zu1oUHs98I4SI2sElybAp1ROiULMupLSBS5722aDkIKU+XOIgVleylIB/tYhoRAr4V4yIyiGAfEStEiCAotQ8OQKpUD4+/vMPgOo2WK6n8O4AAAAASUVORK5CYII=
// @match        https://home.myxplor.com/*
// @require      https://openuserjs.org/src/libs/parlor.trickss/Mutation_Summary.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/jszip/3.5.0/jszip.min.js
// @require      https://gitcdn.xyz/repo/eligrey/FileSaver.js/master/dist/FileSaver.js
// @require      https://raw.githubusercontent.com/Viglino/iconicss/master/iconicss.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js
// @grant        GM_addStyle
// @grant        GM_xmlhttpRequest
// @grant        GM_getResourceText
// @license      MIT
// @copyright    2020, parlortricks
// @version      2.3.0
// ==/UserScript==

var debug = true;
var author = 'parlortricks';
var version = '2.3.0';
var homepage = 'https://openuserjs.org/scripts/parlor.trickss/Xplor_Album_Download';

var albumTitle = '';
var albumSummary = '';

var elmAlbumTitle = 'div.sc-AxhCb.iWRbdJ';
var elmAlbumSummary = 'div.sc-AxhCb.dyobGx';
var elmImages = 'img.sc-AxiKw.ekLvwz';

var cssResourceNames = [
    'https://gitlab.com/ParlorTricks/xplor-album-download/-/raw/master/css/xplor-css.css',
];

function addResourceCSS(cssResourceNames, debug) {
    cssResourceNames.forEach(function (resource) {
        resource = resource + "?" + Math.random();
        GM_xmlhttpRequest({
            method: "GET",
            url: resource,
            onload: function (response) {
                var css = response.responseText;
                GM_addStyle(css);
                //if (debug) { console.debug('done: add ' + css) };
            }
        });
    })
}

function unique(arr) {
    var u = {}, a = [];
    for (var i = 0, l = arr.length; i < l; ++i) {
        if (!u.hasOwnProperty(arr[i])) {
            a.push(arr[i]);
            u[arr[i]] = 1;
        }
    }
    return a;
}

function formatDate(date) {
    var result = '';
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hours = '' + (d.getHours()),
        minutes = '' + (d.getMinutes());

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    if (hours.length < 2)
        hours = '0' + hours;
    if (minutes.length < 2)
        minutes = '0' + minutes;

    result = `${year}${month}${day}_${hours}${minutes}`;

    return result;
}

function docReady(fn) {
    // see if DOM is already available
    if (document.readyState === "complete" || document.readyState === "interactive") {
        // call on next available tick
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

function xplorMain(summaries) {
    if (debug) { console.log("DOM elements we are watching for have been seen.") };
    var imageURLS = [];

    function createDownloadButton(imageURLS) {
        if (debug) { console.log('Creating download button.') };
        var btn_download = document.createElement('Button');
        btn_download.id = 'btn_download';
        btn_download.classList.add('btn_download');
        btn_download.innerHTML = '<span class="icss-stack icss-tada x1" style="margin-right: 5px"><i class="icss-download"></i></span>Download';
        btn_download.onclick = function () { generateZIP(imageURLS) };
        return btn_download
    }

    function generateZIP(imageURLS) {
        /*var doc = new jsPDF({
            orientation: 'p',
            unit: 'mm',
            format: 'a4'
        });
        doc.setFontSize(22);
        doc.setFontType("bold");
        doc.text(albumTitle, 20, 20);
        doc.setFontSize(12);
        var lines = doc.splitTextToSize(albumSummary, 170);
        var y = 30;
        lines.forEach(function (line) {
            doc.text(line, 20, y);
            y += 5;
        });

        doc.save('Test.pdf');*/
        if (debug) { console.log('Generating zip file.') };
        var zip = new JSZip();
        var count = 0;
        var now = formatDate(Date.now())
        var albumTitleFile = albumTitle.split(' ').join('_');
        var zipFilename = now + '_' + albumTitleFile + '.zip';

        imageURLS.forEach(function (url, i) {
            var filename = now + '_' + albumTitleFile + '_' + i.toString() + '.jpg';
            GM_xmlhttpRequest({
                method: "GET",
                url: url,
                responseType: 'arraybuffer',
                onload: function (res) {
                    zip.file(filename, res.response, {
                        binary: true
                    });
                    count++;
                    if (count == imageURLS.length) {
                        zip.file(now + '_' + albumTitleFile + '_' + 'description.txt', albumSummary)
                        zip.generateAsync({
                            type: 'blob',
                            compression: "DEFLATE",
                            compressionOptions: {
                                level: 9
                            },
                            comment: albumTitle
                        }).then(function (content) {
                            if (debug) { console.log('Sending zip file to user.') };
                            saveAs(content, zipFilename);
                        });
                    };
                }
            });
        });

    }
    console.log(summaries);
    if ((summaries[0].added.length > 0) && (summaries[1].added.length > 0) && (summaries[1].added.length > 0)) {


        summaries[0].added.forEach(function (image) {
            imageURLS.push(image.getAttribute('src'));
        });
        imageURLS = unique(imageURLS);
        if (debug) { console.log(imageURLS) };



        albumTitle = summaries[1].added[0].innerText.trim();
        if (debug) { console.log(albumTitle) };


        document.querySelector('div.sc-AxjAm.bsJdAt').appendChild(createDownloadButton(imageURLS)); //attach the button to the button row above the slides, where print is.

        //document.querySelector('#btn_download').setAttribute('data-tooltip', 'Click here to download all the images in the observation as a zip');



        albumSummary = summaries[2].added[0].innerText.trim();
        if (debug) { console.log(albumSummary) };


    }

}


docReady(function () {
    if (debug) { console.log("Document loaded."); };

    console.debug(formatDate(Date.now()));
    addResourceCSS(cssResourceNames, debug);

    if (debug) { console.log("Start monitoring for mutations of the DOM."); };
    var observer = new MutationSummary({
        callback: xplorMain,
        queries: [
            { element: elmImages },
            { element: elmAlbumTitle },
            { element: elmAlbumSummary }
        ]
    });
});
