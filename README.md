# Summary
The script is written for the Child Care Centre website [MyXplor](https://home.myxplor.com/) This script allows you to download the images as a zipfile from your childs observations, and will also include the observation text in a file inside the zip.

# Reason for creation
I noticed the mobile application and the website did not allow you to download the images for keeping. You could only view them on the service. So i went about working out how to grab the images, found the URIs and proceeded to create a userscript that would allow you to download all the images as an album inside a zip file.

It is purely create for personal use, but i wanted to share it with any parents out there whose Child Care Centre may also use this service.