function addResourceCSS(cssNames, debug) {
    for (var x in cssNames) {
        if (debug) { console.time(cssNames[x]) };
        var thetext = GM_getResourceText(cssNames[x]);
        if (debug) { console.timeEnd(cssNames[x]) };
        GM_addStyle(thetext);
    }
}